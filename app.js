const API_KEY = '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';
const submitButton = document.querySelector('#submit');
const outPutElement = document.querySelector('#output');
const inputElement = document.querySelector('input');
const historyElement = document.querySelector('.history');
const buttonElement = document.querySelector('button');

async function getMessage() {
    console.log('Clicked');

    const options = {
        method: 'POST',
        headers: {
            Authorization: `Bearer ${API_KEY}`,
            "Content-Type": "application/json"
        },
        body: JSON.stringify({
            "model": "gpt-3.5-turbo",
            "messages": [{"role": "user", "content": inputElement.value, "name": "Carlos"}]
            // max_tokens: 100
        }) 
    }

    try {
        const response = await fetch('https://api.openai.com/v1/chat/completions', options)
        const data = await response.json();
        console.log(data);
        outPutElement.textContent = data?.choices[0].message.content

        if (data.choices[0].message.content) {
            const pElement = document.createElement('p');
            pElement.textContent = inputElement.value;
            pElement.addEventListener('click', () => changeInput(pElement.textContent))
            historyElement.append(pElement)
        }

    } catch (error) {
        console.log(error)
    }
}


function changeInput(value) {
    const inputElement = document.querySelector('input');
    inputElement.value = value;
}


inputElement.addEventListener('keypress', (event) => {
    if (event.key === 'Enter') {
        getMessage()
    }
})
submitButton.addEventListener('click', getMessage);

function clearInput () {
    inputElement.value = '';
}

buttonElement.addEvenetListener('click', clearInput);